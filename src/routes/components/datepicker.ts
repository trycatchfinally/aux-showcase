import * as moment from 'moment';

export class Datepicker {

  public initial: Date;
  public dateValue: Date;
  public dateTimeValue: Date;
  public timeValue: Date;

  constructor() {
    let d = new Date();
    d.setDate(d.getDate() - 3);

    this.initial       = d;
    //this.dateValue     = d;
    this.dateTimeValue = new Date(d.getTime());
    this.timeValue     = new Date(d.getTime());
  }

  attached() {
    console.log(this.initial);
  }
}

export class DateTimeFormatValueConverter {
  toView(date, format) {
    if (!date) return '';

    let dateParse = moment(date);
    date          = dateParse.isValid() ? dateParse : null;

    return date ? moment(date).format(format) : '';
  }
}
